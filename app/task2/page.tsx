"use client";

import { Images } from "@/types/index";
import { Dialog, DialogPanel } from "@headlessui/react";
import Image from "next/image";
import { useEffect, useState } from "react";

const Task2 = () => {
  const [images, setImages] = useState<Images[]>([]);
  const [isOpen, setIsOpen] = useState(false);
  const [selectedImage, setSelectedImage] = useState<Images | null>(null);

  useEffect(() => {
    const fetchImages = async () => {
      const response = await fetch("/api/images");
      const data = await response.json();
      setImages(data);
    };
    fetchImages();
  }, []);

  const openModal = (image: Images) => {
    setSelectedImage(image);
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
    setSelectedImage(null);
  };

  return (
    <div>
      <h1 className="text-4xl font-bold mb-6">My Favorite Cars</h1>
      <div className="gallery grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 p-4">
        {images.map((image) => (
          <div
            key={image.id}
            className="image bg-white shadow-md rounded-lg overflow-hidden cursor-pointer"
            onClick={() => openModal(image)}
          >
            <Image
              src={image.url}
              alt={image.caption}
              width={500}
              height={500}
              loading="lazy"
              className="object-cover w-full h-64"
            />
            <p className="p-4 text-center text-gray-700">{image.caption}</p>
          </div>
        ))}
      </div>

      {isOpen && selectedImage && (
        <Dialog
          as="div"
          className="fixed inset-0 z-10 overflow-y-auto"
          open={isOpen}
          onClose={closeModal}
        >
          <div className="min-h-screen px-4 text-center">
            <DialogPanel className="fixed inset-0 bg-black opacity-30" />

            <span
              className="inline-block h-screen align-middle"
              aria-hidden="true"
            >
              &#8203;
            </span>

            <div className="inline-block w-full max-w-3xl p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-lg">
              <Image
                src={selectedImage.url}
                alt={selectedImage.caption}
                width={2000}
                height={2000}
                className="object-contain w-full h-124"
              />
              <p className="mt-4 text-center text-gray-700">
                {selectedImage.details}
              </p>
              <button
                onClick={closeModal}
                className="mt-4 w-full inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-blue-600 border border-transparent rounded-md hover:bg-blue-700 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
              >
                Close
              </button>
            </div>
          </div>
        </Dialog>
      )}
    </div>
  );
};

export default Task2;

import TaskCard from "@/components/TaskCard";
import React from "react";

const Home = () => {
  return (
    <div>
      <main className="flex flex-col items-center justify-center min-h-screen py-2 bg-gray-100">
        <h1 className="text-4xl font-bold mb-4">
          Welcome to the Next.js Assessment
        </h1>
        <p className="text-lg text-center mb-8">
          Below are the tasks that showcase the implementation of various
          features in Next.js and TypeScript.
        </p>

        <div className="w-full max-w-4xl">
          <TaskCard
            title="Task #1: Real-time Search Feature"
            description="Develop a simple Next.js application with real-time search functionality within a static list of objects. Use a static JSON file to mock the dataset. Include tests to verify that your solution works."
            link="/task1"
          />
          <TaskCard
            title="Task #2: Image Optimization and Lazy Loading"
            description="Develop a simple photo gallery Next.js application with image optimization and lazy loading techniques. Implement API routes to fetch photo data from a datasource or a mock. Include tests to verify that your solution works correctly under concurrent usage."
            link="/task2"
          />
          <TaskCard
            title="Task #3: Open-ended Challenge"
            description="Propose and solve a programming challenge that showcases your ingenuity and deep understanding of NextJS and Typescript. This could involve algorithms, system design, tool development, or any area you excel in."
            link="/task3"
          />
        </div>
      </main>
    </div>
  );
};

export default Home;

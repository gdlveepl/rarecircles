"use client";

import SearchBar from "@/components/SearchBar";
import "@/styles/globals.css";
import { Data } from "@/types";
import { useEffect, useMemo, useState } from "react";

const Task1 = () => {
  const [datas, setDatas] = useState<Data[]>([]);
  const [query, setQuery] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      const res = await fetch("/data.json");
      const data: Data[] = await res.json();
      setDatas(data);
    };

    fetchData();
  }, []);

  const filteredDatas = useMemo(() => {
    return datas.filter((data) => {
      const dataName = data.name.toLowerCase();
      return dataName.includes(query.toLowerCase());
    });
  }, [datas, query]);

  return (
    <div className="min-h-screen flex flex-col items-center">
      <h1 className="text-4xl font-bold mb-6">List of Noodles</h1>
      <SearchBar {...{ query, setQuery }} />
      <ul className="mt-4 w-1/2 bg-white shadow-lg rounded-lg p-4">
        {filteredDatas.map((data) => (
          <li
            onClick={() => setQuery(data.name)}
            key={data.id}
            className="py-2 border-b last:border-none cursor-pointer"
          >
            {data.name}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Task1;

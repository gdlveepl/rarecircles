import {
  render,
  screen,
  waitFor,
  fireEvent,
  act,
} from "@testing-library/react";
import "@testing-library/jest-dom";
import Task1 from "../page";

global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () =>
      Promise.resolve([
        { id: "1", name: "Shin Ramyeon" },
        { id: "2", name: "Jin Ramyeon (Mild)" },
        { id: "3", name: "Jin Ramyeon (Spicy)" },
        { id: "4", name: "Real Cheese" },
        { id: "5", name: "Stir Fry Mild" },
        { id: "6", name: "Stir Fry Spicy" },
        { id: "7", name: "Xtra Big Calamansi" },
        { id: "8", name: "Xtra Big Sweet & Spicy" },
        { id: "9", name: "Xtra Big Xtra Hot" },
      ]),
  })
) as jest.Mock;

describe("Task1 Component", () => {
  beforeEach(async () => {
    await act(() => render(<Task1 />));
  });

  it("renders correctly with fetched data", async () => {
    await waitFor(() => {
      expect(screen.getByText("List of Noodles")).toBeInTheDocument();
    });

    const items = screen.getAllByRole("listitem");
    expect(items).toHaveLength(9);
  });

  it("filters the list based on search query", async () => {
    await waitFor(() => {
      expect(screen.getByText("List of Noodles")).toBeInTheDocument();
    });

    const searchInput = screen.getByPlaceholderText("Search...");
    fireEvent.change(searchInput, { target: { value: "Ramyeon" } });

    const filteredItems = screen.getAllByRole("listitem");
    expect(filteredItems).toHaveLength(3);
  });

  it('clears the search query when "X" button is clicked', async () => {
    await waitFor(() => {
      expect(screen.getByText("List of Noodles")).toBeInTheDocument();
    });

    const searchInput = screen.getByPlaceholderText("Search...");
    fireEvent.change(searchInput, { target: { value: "Ramyeon" } });

    const clearButton = screen.getByText("X");
    fireEvent.click(clearButton);

    expect(searchInput).toHaveValue("");
  });
});

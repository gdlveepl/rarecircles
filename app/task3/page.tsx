const Task3 = () => {
  return (
    <div className="h-screen flex flex-col justify-center">
      <section className="w-full bg-blue-500 text-white py-20">
        <div className="max-w-7xl mx-auto text-center">
          <h1 className="text-5xl font-bold mb-4">
            I&apos;ll send Task 3 in a different repository
          </h1>
        </div>
      </section>
    </div>
  );
};

export default Task3;

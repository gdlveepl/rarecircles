import Nav from "@/components/Nav";
import "@/styles/globals.css";

export const metadata = {
  title: "Rare Circles Challenge",
  description: "My Repository for the rarecircles challenge",
};

const RootLayout = ({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) => {
  return (
    <html lang="en">
      <body>
        <main>
          <Nav />
          <div className="bg-gray-100 p-10">{children}</div>
        </main>
      </body>
    </html>
  );
};

export default RootLayout;

import { NextResponse } from "next/server";
import { Images } from "@/types";

const images: Images[] = [
  {
    id: "1",
    url: "/images/evo4.jpg",
    caption: "Mitsubishi Lancer Evolution 4",
    details:
      "The Mitsubishi Lancer Evolution IV, or Evo 4, was produced from 1996 to 1998. It featured a 2.0-liter turbocharged inline-4 engine, producing around 276 horsepower and 260 lb-ft of torque. It had a revised all-wheel-drive system and improved handling dynamics. The Evo 4 also introduced active yaw control, which greatly enhanced its cornering capabilities.",
  },
  {
    id: "2",
    url: "/images/evo5.jpg",
    caption: "Mitsubishi Lancer Evolution 5",
    details:
      "The Mitsubishi Lancer Evolution V, or Evo 5, was produced from 1998 to 1999. It carried forward the 2.0-liter turbocharged inline-4 engine, with similar power output as the Evo 4 but featured significant upgrades in aerodynamics, cooling, and suspension. The Evo 5 had wider tracks and a more aggressive look, making it a popular choice among rally enthusiasts.",
  },
  {
    id: "3",
    url: "/images/m3.jpg",
    caption: "BMW M3",
    details:
      "The BMW M3 is a high-performance version of the BMW 3 Series, developed by BMW's in-house motorsport division, BMW M. The M3 has gone through several generations since its introduction in 1986. Known for its balance of power and handling, it typically features a high-revving inline-6 engine, although other engines have been used in different generations. It's renowned for its precision and driver engagement.",
  },
  {
    id: "4",
    url: "/images/nsx.jpg",
    caption: "Honda NSX",
    details:
      "The Honda NSX, also known as the Acura NSX in North America, is a mid-engine sports car that was first produced in 1990. The first generation featured a 3.0-liter V6 engine with VTEC, producing around 270 horsepower. It was known for its lightweight aluminum body, exceptional handling, and everyday usability. The NSX was a technological marvel, blending performance with reliability.",
  },
  {
    id: "5",
    url: "/images/r34.jpg",
    caption: "Nissan GTR R34",
    details:
      "The Nissan GTR R34, produced from 1999 to 2002, is one of the most iconic Japanese sports cars. It features a 2.6-liter twin-turbocharged inline-6 engine, known as the RB26DETT, producing around 276 horsepower (although it was often underrated). The R34 is renowned for its advanced ATTESA E-TS all-wheel-drive system and Super-HICAS four-wheel steering, making it a formidable performer on both the road and track.",
  },
  {
    id: "6",
    url: "/images/rx7.jpg",
    caption: "Mazda RX7",
    details:
      "The Mazda RX7 is a legendary sports car known for its rotary engine. The third generation, known as the FD, was produced from 1992 to 2002. It featured a 1.3-liter twin-turbocharged rotary engine, producing around 276 horsepower. The RX7 is celebrated for its lightweight construction, balanced chassis, and unique engine characteristics, offering a thrilling and distinctive driving experience.",
  },
  {
    id: "7",
    url: "/images/shellby.jpg",
    caption: "Ford Shellby GT500 Eleanor ",
    details:
      'The Ford Shelby GT500 Eleanor is a modified version of the 1967 Mustang Fastback, famously featured in the movie "Gone in 60 Seconds." The car is equipped with a powerful V8 engine, producing over 400 horsepower. Eleanor is known for its aggressive styling, including a custom body kit, side exhausts, and distinctive stripes. It\'s a blend of classic muscle car aesthetics and modern performance upgrades.',
  },
];

export async function GET() {
  return NextResponse.json(images);
}

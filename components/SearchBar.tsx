import { Dispatch, SetStateAction, useCallback } from "react";

export type Props = {
  query: string;
  setQuery: Dispatch<SetStateAction<string>>;
};

const SearchBar = ({ query, setQuery }: Props) => {
  const handleClear = useCallback(() => {
    setQuery("");
  }, [setQuery]);

  return (
    <div className="relative w-1/2">
      <input
        type="text"
        value={query}
        onChange={(e) => setQuery(e.target.value)}
        placeholder="Search..."
        className="w-full p-2 border border-gray-300 rounded-lg shadow-sm focus:outline-none focus:ring-2 focus:ring-blue-500"
      />
      {query && (
        <button
          onClick={handleClear}
          className="mt-1 mr-1 absolute inset-y-0 right-0 flex items-center justify-center w-8 h-8 bg-gray-200 rounded-full text-gray-600 hover:text-gray-800 focus:outline-none"
        >
          X
        </button>
      )}
    </div>
  );
};

export default SearchBar;

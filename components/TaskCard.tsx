import Link from "next/link";

type Props = {
  title: string;
  description: string;
  link: string;
};

const TaskCard = ({ title, description, link }: Props) => (
  <div className="bg-white shadow-md rounded-lg p-6 mb-6">
    <h2 className="text-2xl font-semibold mb-2">{title}</h2>
    <p className="text-gray-700 mb-4">{description}</p>
    <Link className="text-blue-500 hover:underline" href={link}>
      Go to Task
    </Link>
  </div>
);

export default TaskCard;

import Link from "next/link";

const Nav = () => {
  const navItems = [
    { id: "1", label: "Home", link: "/" },
    { id: "2", label: "Task 1", link: "/task1" },
    { id: "3", label: "Task 2", link: "/task2" },
    { id: "4", label: "Task 3", link: "/task3" },
  ];

  return (
    <nav className="bg-blue-500 p-4">
      <div className="max-w-7xl mx-auto flex justify-between items-center">
        <div className="flex items-center space-x-4">
          <span className="text-white text-xl font-bold">Gurg</span>
        </div>
        <div className="md:flex space-x-4">
          {navItems.map((item) => (
            <Link
              className="text-white hover:text-gray-200"
              key={item.id}
              href={item.link}
            >
              {item.label}
            </Link>
          ))}
        </div>
      </div>
    </nav>
  );
};

export default Nav;

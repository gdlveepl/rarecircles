1. **Real Time Search**

   ```
   # Things to Improve
    Here are the things than can be improved on my implementation, 1. Have Loading and Error Handling on Data Fetching so that user can properly identify the current status of the application.
    2. Have the Data Fetched in an API Route
    3. Add Loading and Error Handling on the test
    4. I am still studying the aria attributes maybe add them here as well.
    5. Add on click modals to view the details of the searched Item
    6. debouncing a button is still new to me but still a good idea to prevent multiple clicks!
    7. Improve the Unit Test!
   ```

2. **Image optimization and Lazy loading**

   ```
   # Things to Improve
    Here are the things than can be improved on my implementation, 1. Have Loading and Error Handling on Data Fetching so that user can properly identify the current status of the application.
    2. Utilize useMemo and useCallBack on this Module
    3. Add Loading and Error Handling on the test
    4. Ensure the modal and interactive elements have appropriate ARIA attributes for better accessibility.
    5. Expand tests to cover more edge cases and interactions. Ensure the tests handle cases where the fetch might fail or the data is incomplete.
    6. Maybe utilize the placeholder blur feature of next/image
    7. give fallback image for missing files
    8. debouncing a button is still new to me but still a good idea to prevent multiple clicks!
    9. Improve the Unit Test!
    10. Make the modal a Component
   ```

3. **open-ended challenge**
   Hi Pietro! will send this in a separate repository!

   Thank you!

export type Data = {
  id: string;
  name: string;
};

export type Images = {
  id: string;
  url: string;
  caption: string;
  details: string;
};
